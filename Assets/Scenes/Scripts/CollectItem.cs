﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectItem : MonoBehaviour
{
    
    // Start is called before the first frame update
    void Awake()
    {

    }

    // Update is called once per frame
    void OnTriggerEnter(Collider plyr)
    {
        if(plyr.gameObject.tag == "Player")
        {
            gameObject.SetActive(false);
        }
    }
}
