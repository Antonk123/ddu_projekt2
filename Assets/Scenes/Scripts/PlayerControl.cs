﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Photon.Pun;
using UnityEngine;

public class PlayerControl : MonoBehaviourPunCallbacks
{
    public float speed;
    public float rotSpeed;
    public float jumpForce;
    [SerializeField] private Camera GetCamera;
    private PhotonView PhotonView;
    private Rigidbody rb;
    public bool jumping;

    public Transform footArea;
    public LayerMask groundLayer;
    private int count;
    public Text countText;

    public Animator anim;
    private Vector3 jumpVector;

    private void Awake()
    {
        jumpVector.y = jumpForce;
        jumping = false;
        PhotonView = GetComponent<PhotonView>();
        count = 0;
    }

    public void Start()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();

        if (photonView.IsMine)
        {
            GetCamera.gameObject.SetActive(true);
            countText = GameObject.FindGameObjectWithTag("PointCounter").GetComponent<Text>();
            countText.text = "Points: 0";
        }
    }

    private void Update()
    {
        //photonView.ismine checker om characteren er din og hvis det er det så gøre den functionen.
        // den gøre på 2 aksler hvor det er den finder inputet for "Horizontal" og "Vertical", den horizontale er vores rotation og vertical er det som gøre vi bevæger os frem og tilbage samt side til side. 
        if (PhotonView.IsMine)
        {
           /* if (Input.GetButtonDown())
            {

            } */

            float x = Input.GetAxis("Horizontal") * Time.deltaTime * rotSpeed;
            float z = Input.GetAxis("Vertical") * Time.deltaTime * speed;

            //transform rotate beskriver hvilken axe vi skal rotere 
            //transform.translate tager så vores float variable og siger hvilken axe vores speed er.
            transform.Rotate(0, x, 0);
            transform.Translate(0, 0, z);

            if (Input.GetButtonDown("Jump"))
            {

                if (Physics.OverlapSphere(footArea.position, 0.15f, groundLayer).Length > 0)
                {

                    rb.AddForce(jumpVector);
                    //rb.velocity = new Vector3(0, jumpForce * Time.deltaTime, 0);
                    Debug.Log("Jumped");
                    jumping = true;
                    anim.SetBool("IsJumping", true);
                }
                anim.SetBool("IsJumping",false);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (PhotonView.IsMine)
        {
            if (other.gameObject.tag == "PickUpC")
            {
                other.gameObject.SetActive(false);
                count++;
  //              Debug.Log("Coin picked up!");

                SetCountText();
            }
            if (other.gameObject.tag == "PickUpD")
            {
                other.gameObject.SetActive(false);
                count += 5;
//                Debug.Log("Diamond picked up!");

                SetCountText();
            }
        }
    }

    public void SetCountText()
    {
        //countText.text = "Points: " + count.ToString();
        countText.text = "Points: " + count.ToString();
    }
}
