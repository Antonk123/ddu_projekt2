﻿using Photon.Pun;
using UnityEngine;

public class QuickStartRoomControl : MonoBehaviourPunCallbacks
{
    [SerializeField]
    //private int TestLevelScene;
    //number for the build index to the multiplayer scene.

    public override void OnEnable()
    {
        PhotonNetwork.AddCallbackTarget(this);
    }

    public override void OnDisable()
    {
        PhotonNetwork.RemoveCallbackTarget(this);
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("Joined room");
        StartGame();
    }

    private void StartGame()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            Debug.Log("Joining Room");

            PhotonNetwork.LoadLevel("Level1");
        }
    }


}
