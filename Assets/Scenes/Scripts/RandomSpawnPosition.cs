﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawnPosition : MonoBehaviour
{
    // Prefabs to instantiate
    public GameObject coin, diamond;

    // Spawn prefrabs one per 2 seconds
    public float spawnTime = 0.4f;

    // Variable to contain random value
    float timer = 0;

    // Variable to contain random value
    int whatToSpawn;

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer > spawnTime)
        {
            SpawnRandom();
            timer = 0;
        }
    }

    public void SpawnRandom()
    {
        Vector3 level = new Vector3(Random.Range(-34,34),10,Random.Range(-34,20));
        whatToSpawn = Random.Range(1, 5); // define random value between 1 and 5 (6 is exclusive)
                                          //Debug.Log(whatToSpawn); // display its value in console
        // instantiate a prefab depending on random vlaue
        switch (whatToSpawn)
        {
            case 1:
                Instantiate(coin, level, Quaternion.identity);
                break;
            case 2:
                Instantiate(coin, level, Quaternion.identity);
                break;
            case 3:
                Instantiate(coin, level, Quaternion.identity);
                break;
            case 4:
                Instantiate(diamond, level, Quaternion.identity);
                break;
            case 5:
                Instantiate(diamond, level, Quaternion.identity);
                break;
        }
    }
}
