﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class GameSetupController : MonoBehaviourPunCallbacks
{
    public GameObject coin, diamond;
    public float spawnTime = 0.4f;
    float timer = 0;
    //public PlayerControl pc;
    int whatToSpawn;
    
    private void Update()
    {
        timer += Time.deltaTime;
        if (timer > spawnTime)
        {
            CreateSpawner();
            timer = 0;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        CreatePlayer();
        //PlayerControl pc = new PlayerControl();
        //pc.SetCountText();
    }

    private void CreatePlayer()
    {
        Debug.Log("Creating Player");
        PhotonNetwork.Instantiate(Path.Combine
        ("PhotonPrefab", "Captain"),
        Vector3.zero, Quaternion.identity);

    }

    private void CreateSpawner()
    {
        Vector3 level = new Vector3(Random.Range(-34, 34), 0, Random.Range(-34, 20));
        whatToSpawn = Random.Range(1, 5);

        switch (whatToSpawn)
        {
            case 1:
                Instantiate(coin, level, Quaternion.identity);
                break;
            case 2:
                Instantiate(coin, level, Quaternion.identity);
                break;
            case 3:
                Instantiate(coin, level, Quaternion.identity);
                break;
            case 4:
                Instantiate(diamond, level, Quaternion.identity);
                break;
            case 5:
                Instantiate(diamond, level, Quaternion.identity);
                break;
        }
    }

    public void Counter()
    {
        //GameObject counter = GameObject.Find("PlayerControl");
        
        //Text t = pc.countText;
        //PlayerControl  = counter.GetComponent.gameObject();
    }

}
