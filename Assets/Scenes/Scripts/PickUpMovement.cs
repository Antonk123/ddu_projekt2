﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpMovement : MonoBehaviour
{
    //adjust this to change how the speed of the hover
    public float flySpeed;
    //adjust this to change how high it goes
    public float height;
    //adjust this to change how the speed of the rotation
    public float rotateSpeed;

    private void Start()
    {
        
        //transform.Rotate(Vector3);
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Ground")
        {
            Update();
        }
    }

    private void Update()
    {
        //get the objects current position and put it in a variable so we can access it later with less code
        Vector3 pos = transform.position;
        //calculate what the new Y position will be
        float newY = Mathf.Sin(Time.time * flySpeed);
        //set the object's Y to the new calculated Y
        transform.position = new Vector3(pos.x, newY * height, pos.z);
        transform.Rotate(new Vector3(0, 0, rotateSpeed));
    }
}
