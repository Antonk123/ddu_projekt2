﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Photon.Pun;
using UnityEngine;

public class PlayerMovement : MonoBehaviourPunCallbacks
{
    public float speed;
    public float jumpForce;

    private Rigidbody rb;

    private bool jumping = false;

    private int count;
    private int cPoint = 1;
    private int dPoint = 5;
    public Text countText;

    private PhotonView PhotonView;

    private void Awake()
    {
        PhotonView = GetComponent<PhotonView>();
    }

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        //rb.freezeRotatio

    }

    void Update()
    {
        if (photonView.IsMine)
        {
            Camera.main.GetComponent<MoveCam>().player = this.gameObject;
        }

        if (PhotonView.IsMine)
        {
            float mHori = Input.GetAxis("Horizontal");
            float mVert = Input.GetAxis("Vertical");

            rb.velocity = new Vector3(mHori * speed, rb.velocity.y, mVert * speed);
            transform.Rotate(new Vector3(0, mHori * Time.deltaTime, 0));

            if (Input.GetButtonDown("Jump"))
            {
                if (rb.velocity.y == 0)
                {
                    rb.velocity = new Vector3(0, jumpForce, 0);
                }
                jumping = true;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "PickUpC")
        {
            other.gameObject.SetActive(false);
            count++;
            SetCountText();
        }
        if (other.gameObject.tag == "PickUpD")
        {
            other.gameObject.SetActive(false);
            count++;
            count++;
            count++;
            count++;
            count++;

            SetCountText();
        }
    }

    void SetCountText()
    {
        countText.text = "Points: " + count.ToString();
    }
}
