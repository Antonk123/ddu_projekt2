﻿﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkController : MonoBehaviourPunCallbacks
{
    // Setting up connection between photon settings and this class
    void Start()
    {
        PhotonNetwork.ConnectUsingSettings();
    }
    // Logging which server the game is connected to
    public override void OnConnectedToMaster()
    {
        Debug.Log("You are connected to " + PhotonNetwork.CloudRegion);
    }

    delegate void CountdownTimerHasExpired();
    // Unneeded in this class.
    void Update()
    {
        
    }
}
