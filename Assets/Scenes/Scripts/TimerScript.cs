﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerScript : MonoBehaviour
{
    // Variable to contain when the game should start
    float timerStart = 10;

    // Variable to contain time of the game
    float timer = 0;

    // Variable to contain when the game ends
    float timerEnd = 60;

    private void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer == timerEnd)
        {
            
            
        }
    }
}
