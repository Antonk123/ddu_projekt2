﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuickStartLobbyController : MonoBehaviourPunCallbacks
{
    [SerializeField]
    //button for creating and joining game
    private GameObject quickStartButton;

    [SerializeField]
    //button that cancels game search
    private GameObject quickCancelButton;

    [SerializeField]
    //button that cancels game search
    private GameObject quickLoadingButton;

    [SerializeField]
    //Manual set the number of players in the room
    public int RoomPlayerAmount;

    public override void OnConnectedToMaster()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        //Makes it so whatever scene the master client has
        //loaded is the scene all other clients will load
        quickStartButton.SetActive(true);
        quickLoadingButton.SetActive(false);
    }

    public void QuickStart() //Paired to the quick start button
    {
        quickStartButton.SetActive(false);
        quickCancelButton.SetActive(true);
        PhotonNetwork.JoinRandomRoom();

        Debug.Log("Quick Start");
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log("Failed to join a room");
        CreateRoom();
    }

    void CreateRoom()
    {
        int randomRoomNumber = Random.Range(0, 100);
        RoomOptions roomOps = new RoomOptions()
        {
            IsVisible = true,
            IsOpen = true,
            MaxPlayers = (byte)RoomPlayerAmount
        };

        PhotonNetwork.CreateRoom("Room" + randomRoomNumber, roomOps);
        
        Debug.Log(randomRoomNumber);
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("Faild to create... trying again");
        CreateRoom();
        //Retrying to create a new room with different name
    }

    public void QuickCancel()
    {
        quickCancelButton.SetActive(false);
        quickStartButton.SetActive(true);
        PhotonNetwork.LeaveRoom();
    }
}
