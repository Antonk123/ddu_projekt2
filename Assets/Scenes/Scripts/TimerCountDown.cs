﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class NewBehaviourScript : MonoBehaviourPunCallbacks
{
    private float startTime = 10;
    public static string TimeCountdown;
    string timestring;
    // Use this for initialization
    void Start()
    {
        startTime = 180;
    }

    // Update is called once per frame
    void Update()
    {
       if (PhotonNetwork.CountOfPlayersInRooms >= 2)
        {
            float t = 0;
            t = Time.time;
            int min = 3 - ((int)t / 60);
            int sec = 59 - ((int)t % 60);
            string minutes = min.ToString();
            string seconds = sec.ToString();
            timestring = minutes + ":" + seconds;
            //photonView.RPC("TimeLeft", , timestring);
            TimeCountdown = TimeLeft(timestring);

        }
        else
        {
            TimeCountdown = "--:--";
        }
    }

    [PunRPC]
    public string TimeLeft(string timeleft)
    {

        string receivedString = timeleft;
        Debug.Log(receivedString);
        return receivedString;
    }
}
