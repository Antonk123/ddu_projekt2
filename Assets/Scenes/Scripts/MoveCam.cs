﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class MoveCam : MonoBehaviourPunCallbacks
{
    
    /*private void Start()
    {
    }*/
    
    public GameObject player;

    public Vector3 offset = new Vector3(5, 3, 0);
    //public float s;
    // Start is called before the first frame update

    // Update is called once per frame
    void LateUpdate()
    {

        transform.position = player.transform.position + offset;
        //transform.rotation = player.transform.Rotate;
    }
}
